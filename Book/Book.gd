extends Sprite

var selectable = true
var firstPos

enum COLOR {
	R,
	B,
	G,
}

var color
var draggable = false


func isIn(point : Vector2, rectSize : Vector2, rectPos : Vector2):
	return (point.y >= rectPos.y - rectSize.y and point.y <= rectPos.y + rectSize.y
	and point.x >= rectPos.x - rectSize.x and point.x <= rectPos.x + rectSize.x)

func _ready():
	$Area2D.connect("area_entered", self, "enter", []) # When dragged on Bookshelf, enter func activates ONCE
	
	randomize() # Randomize Seed on Run
	color = ["R", "B", "G"][rand_range(0,3)] # Choose one color
	
	match color:
		"R":
			modulate = Color.red
		"B":
			modulate = Color.blue
		"G":
			modulate = Color.limegreen
	
	if !selectable: # If decorative process is not active
		set_process(false)

func _process(delta):
	if Input.is_mouse_button_pressed(1) and isIn(get_global_mouse_position(), get_rect().size, global_position) and draggable:
		rotation_degrees = 0
		position += get_local_mouse_position() # Drag to mousePos
	else:
		position = firstPos
		rotation_degrees = 90
		
func enter(body):
	if color == body.get_parent().color:
		#get_parent().remove_child(self)
		call_deferred("reparent", self, body.get_node("../Books"))
		self.scale = Vector2(1, 1)
		set_process(false)
		
		self.position.y = 2
		self.position.x = 12 * (len( body.get_node("../Books").get_children()) - 1) - 26
		
		$Area2D/CollisionShape2D.set_deferred("disabled", true)
		
		#body.get_node("../Books").call_deferred("add_child", self)

		
func reparent(node, newparent):
	get_parent().remove_child(node)
	newparent.add_child(node)

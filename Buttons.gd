extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Play.connect("button_down", get_tree(), "change_scene", ["res://PreGame.tscn"])
	$Credits.connect("button_down", get_tree(), "change_scene", ["res://Credits.tscn"])

extends Sprite

var bookAmount = 3

func _ready():
	randomize()
	bookAmount = rand_range(4,7)
	for i in bookAmount:
		var b = preload("res://Book/Book.tscn").instance()
		b.position = Vector2((i * 12) - 38, 2)
		b.selectable = false
		add_child(b)

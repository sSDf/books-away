extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var colors = ["R","G","B"]
	colors.shuffle()
	var shelfs = ["1","2","3"]
	shelfs.shuffle()
	for i in len(colors):
		get_node("Shelf" + str(i + 1)).color = colors[i]
		get_node("Shelf" + str(i + 1) + "/Label").text = colors[i]

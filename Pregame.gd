extends Node2D

var Mode : String = ""

var Random : bool



func _ready():
	$Countdown.connect("button_down", self, "countdown")
	$Stopwatch.connect("button_down", self, "stopwatch")
	$Countdown/Random.connect("button_down", self, "random")
	
	$Play.connect("button_down", self, "play")
	$Back.connect("button_down", get_tree(), "change_scene", ["res://Title.tscn"])
	

func _process(delta):
	if $Stopwatch.pressed or $Countdown.pressed:
		$Play.visible = true
	else:
		$Play.visible = false


func countdown():
	$Stopwatch.pressed = false
	$Countdown/Random.visible = !$Countdown/Random.visible
	
	Mode = "Countdown"
	
func stopwatch():
	$Countdown.pressed = false
	$Countdown/Random.visible = false
	
	Mode = "Stopwatch"

func random():
	$Countdown/Random/Time.visible = !$Countdown/Random/Time.visible
	
	Random = !Random

func play():
	var game = preload("res://Game.tscn").instance()
	var time = float($Countdown/Random/Time.text)
	game.get_node("Timer").time = time
	if Random or time == 0:
		randomize()
		print(Random)
		game.get_node("Timer").time = rand_range(15, 40)
		print(game.get_node("Timer").time)
	game.get_node("Timer").mode = Mode
	
	var newGame = PackedScene.new()
	newGame.pack(game)
	
	get_tree().change_scene_to(newGame)
	
	
	

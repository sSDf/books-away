extends Node2D


var bookAmount = 0
var hLimit = 8
var intercept = 20

func _ready():
	randomize()
	bookAmount = rand_range(6, 18)
	for i in bookAmount:
		var b = preload("res://Book/Book.tscn").instance()
		b.rotation_degrees = 90
		b.position.y = -(i * 12) - intercept
		b.position.x = 12
		if i > hLimit:
			b.position.x -= 32
			b.position.y += (hLimit * 12) + 12
		b.firstPos = b.position
		add_child(b)


func _process(delta):
	if !get_children().empty():
		get_children()[-1].draggable = true
		#WIN
	elif get_children().empty():
		Global.emit_signal("win")

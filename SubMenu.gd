extends TextureRect


var won : bool


func _ready():
	visible = false
	Global.connect("win", self, "won", [], CONNECT_ONESHOT)
	Global.connect("lose", self, "lost", [], CONNECT_ONESHOT)
	
	#$PlayAgain.connect("button_down", get_tree(), "reload_current_scene")
	$Back.connect("button_down", self,"back", ["res://PreGame.tscn"])


func won():
	get_tree().paused = true
	var m = get_node("../Timer").mode
	var t
	match m:
		"Stopwatch":
			t = ceil(get_node("../Timer").time2)
			$TimeLeft.text = "You finished in " + str(t) + " seconds."
		"Countdown":
			$TimeLeft.text = "With " + str(t) + " seconds remaining."
			t = ceil(get_node("../Timer").time)
	
	$WonLost.text = "You Won!"
	
	visible = true

func lost():
	get_tree().paused = true
	
	var books = get_node("../Table/Stack").get_child_count()
	$WonLost.text = "You Lost!"
	$TimeLeft.text = "You had "+ str(books) +" books remaining."
	
	visible = true
	
func back(path):
	get_tree().paused = false
	get_tree().change_scene(path)

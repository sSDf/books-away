extends Sprite

export(String, "Countdown", "Stopwatch") var mode

export(float) var time
var time2 : float = 0

signal oneshot

func _ready():
	
	if mode == "Countdown":
		connect("oneshot", self, "timeout", [], CONNECT_ONESHOT)


func _process(delta):
	if mode == "Stopwatch":
		time2 += delta
		$Label.text = str(ceil(time2))
	
	
	if mode == "Countdown":
		$Label.text = str(ceil(time))
		if time <= 0:
			$Label.text = "0"
			emit_signal("oneshot")
		else:
			time -= delta

func timeout():
	set_process(false)
	Global.emit_signal("lose")
